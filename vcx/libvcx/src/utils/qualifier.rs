use regex::Regex;

lazy_static! {
    pub static ref REGEX: Regex = Regex::new("^(did|schema|creddef)(:(indy|cheqd))?(:[a-z0-9]+)?:(.*)$").unwrap();
}

pub fn qualify(prefix: &str, method: &str, entity: &str) -> String {
    format!("{}:{}:{}", prefix, method, entity)
}

pub fn is_fully_qualified(entity: &str) -> bool {
    trace!("is_fully_qualified: captures {:?}", REGEX.captures(entity));
    REGEX.is_match(&entity)
}

pub fn network(entity: &str) -> Option<String> {
    match REGEX.captures(entity) {
        None => None,
        Some(caps) => {
            trace!("qualifier::method: caps {:?}", caps);
            match (caps.get(2), caps.get(3)) {
                (Some(type_), Some(subnet)) => {
                    Some(type_.as_str().to_owned() + subnet.as_str())
                }
                (Some(type_), None) => Some(type_.as_str().to_owned()),
                _ => {
                    warn!("Unrecognized FQ method for {}, parsed items are \
                    (where 2nd is method type, and 3rd is sub-method (namespace, ledger, type, etc)\
                     {:?}", entity, caps);
                    None
                }
            }
        },
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn is_fully_qualified_works() {
        assert!(is_fully_qualified("did:indy:some"));
        //FIXME restore test case: assert!(!is_fully_qualified("did:indy"));
        assert!(!is_fully_qualified("indy:some"));
    }
}